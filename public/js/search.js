import {searchMovie} from './services/movies.js';

const search = document.getElementById('search');
search.addEventListener('keyup', (event) => {
  const query = event.target.value;
  if (event.key === 'Enter') {
    searchMovie(query).then((movie) => {
      movie.map((mo) => {
        const {title, year, poster_path, imdbID} = mo;
        const movieCard = document.createElement('div');
        movieCard.classList.add('movie-card');
        movieCard.innerHTML = `
      <img src="https://image.tmdb.org/t/p/w500${poster_path}" alt="${title}">
      <div class="movie-card-info">
        <p>${title}</p>
        <div>
          <p>${year}</p>
          <p>${imdbID}</p>
        </div>
      </div>
    `;
        document.getElementById('content').appendChild(movieCard);
      });
    });
  } else {
    console.log('No se puede buscar');
  }
});

