import {initializeApp} from 'https://www.gstatic.com/firebasejs/9.5.0/firebase-app.js';
import {getAuth, signInWithEmailAndPassword, signInWithPopup, GoogleAuthProvider} from 'https://www.gstatic.com/firebasejs/9.5.0/firebase-auth.js';

import {firebaseConfig} from '../config/firebase.js';
import '../config/firebase.js';


initializeApp(firebaseConfig);

// const form = document.getElementById('form').elements;
const btn = document.getElementById('btn');
const btnGoogle = document.querySelector('.btn-google');
const auth = getAuth();
const provider = new GoogleAuthProvider();

const loginUser = async (email, password) => {
  try {
    await signInWithEmailAndPassword(auth, email, password);
    Swal.fire({
      title: 'Successfull',
      icon: 'success',
    }).then((result) => {
      window.location.href = 'movie.html';
    });
  } catch (error) {
    Swal.fire({
      title: 'Oops...',
      icon: 'error',
    });
  }
};
const loginGoogle = async () => {
  const login = await signInWithPopup(auth, provider);
  console.log(login);
};

btn.addEventListener('click', (e) => {
  try {
    e.preventDefault();
    const email = document.getElementById('email').value;
    const password = document.getElementById('pass').value;
    loginUser(email, password);
  } catch (error) {
    console.log(error);
  }
}, false);

btnGoogle.addEventListener('click', (e) => {
  try {
    e.preventDefault();
    loginGoogle();
  } catch (error) {
    console.log(error);
  }
});
