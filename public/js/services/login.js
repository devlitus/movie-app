import { getAuth, GoogleAuthProvider, signInWithEmailAndPassword } from 'https://www.gstatic.com/firebasejs/9.5.0/firebase-auth.js';

const auth = getAuth();
const provider = new GoogleAuthProvider();

export const loginUser = async (email, password) => {
  console.log(email, password);
  const user = await signInWithEmailAndPassword(auth, email, password);
  console.log(user);
  return user;
};

export const googleLogin = async () => {
  const user = await auth.signInWithPopup(provider);
  console.log(user);
  return user;
};
